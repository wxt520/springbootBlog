package com.wxt.springbootBlog.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description:
 *
 * @Author Lucky
 * @Date 2018年5月29日 -下午2:28:18
 */
@RestController
public class HelloController {
	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	public String hello() {
		return "Hello world!";
	}
}
