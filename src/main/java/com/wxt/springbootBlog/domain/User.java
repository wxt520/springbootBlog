package com.wxt.springbootBlog.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * 
 * Description: User 实体
 *
 * @Author Lucky
 * @Date 2018年5月30日 -下午4:17:57
 */
// @XmlRootElement // mediatype 转为xml
@Entity
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id; // 用户的唯一标识
	@Column(nullable = false)
	private String name;
	@Column(nullable = false)
	private Integer age;

	// JPA 的规范要求无参构造函数；设为 protected 防止直接使用
	protected User() {
	}

	public User(String name, Integer age) {
		this.name = name;
		this.age = age;
	}

	public User(long id, String name, Integer age) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		// return "User [id=" + id + ", name=" + name + ", age=" + age + "]";

		return String.format("User[id=%d, name='%s', age='%d']", id, name, age);
	}

}
