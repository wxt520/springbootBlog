package com.wxt.springbootBlog.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.wxt.springbootBlog.domain.User;

public interface UserRepository extends CrudRepository<User, Long> {
	// 继承了CrudRepository，他有默认的实现方法，不需要我们实现了
	// /**
	// * 创建或修改用户
	// *
	// * @param user
	// * @return
	// */
	// User saveOrUpdateUser(User user);
	//
	// /**
	// * 删除用户
	// *
	// * @param id
	// */
	// void deleteUser(long id);
	//
	// /**
	// * 根据id查询用户
	// *
	// * @param id
	// * @return
	// */
	// User getUserById(long id);
	//
	// /**
	// * 用户列表
	// *
	// * @return
	// */
	// List<User> listUser();

}
