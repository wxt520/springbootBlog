package com.wxt.springbootBlog.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Repository;

import com.wxt.springbootBlog.domain.User;

@Repository
public abstract class UserRepositoryImpl implements UserRepository {
	// 不需要这个实现方法了
//	private static AtomicLong counter = new AtomicLong();
//	// 存储用户的信息
//	private final ConcurrentMap<Long, User> userMap = new ConcurrentHashMap<Long, User>();
//
//	public UserRepositoryImpl() {
//		User user = new User(null, null);
//		user.setAge(30);
//		user.setName("Way Lau");
//		this.saveOrUpdateUser(user);
//	}
//
//	@Override
//	public User saveOrUpdateUser(User user) {
//		Long id = user.getId();
//		if (id <= 0) {
//			id = counter.incrementAndGet();
//			user.setId(id);
//		}
//		this.userMap.put(id, user);
//		return user;
//	}
//
//	@Override
//	public void deleteUser(long id) {
//		this.userMap.remove(id);
//	}
//
//	@Override
//	public User getUserById(long id) {
//		return this.userMap.get(id);
//	}
//
//	@Override
//	public List<User> listUser() {
//		return new ArrayList<User>(this.userMap.values());
//	}

}
